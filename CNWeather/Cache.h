#pragma once

#ifndef RM_PLUGIN_CACHE_H
#define RM_PLUGIN_CACHE_H

#include <string>

class Cache {
public:
	Cache();
	virtual ~Cache();
	virtual std::wstring read(const std::string& key) = 0;
	virtual bool write(const std::string& key, std::wstring& content) = 0;

	time_t getResTime() const;

protected:
	void setResTime(time_t resTime);

private:
	time_t mResTime;
};

class DiskCache : public Cache {
public:
	DiskCache();
	~DiskCache();
	virtual std::wstring read(const std::string& key) override;
	virtual bool write(const std::string& key, std::wstring& content) override;

private:
	bool isCacheLive(time_t saveTime) const;
	std::string buildPath(const std::string& key) const;

private:
	bool mEnableCache;
	std::string mCacheDir;
	CRITICAL_SECTION mCriticalSection;
};

#endif // !RM_PLUGIN_CACHE_H