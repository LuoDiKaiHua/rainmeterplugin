#pragma once

#ifndef RM_PLUGIN_LOCK_H
#define RM_PLUGIN_LOCK_H

#include <Windows.h>

class AutoLock {
public:
	AutoLock(CRITICAL_SECTION *pCriticalSection);
	~AutoLock();

private:
	CRITICAL_SECTION *mCriticalSection;
};

#endif RM_PLUGIN_LOCK_H