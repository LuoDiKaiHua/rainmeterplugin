#pragma once

#ifndef RM_PLUGIN_JSONPARSER_H
#define RM_PLUGIN_JSONPARSER_H

#include <string>
#include "../cJson/cJSON.h"

class Measure;

class JsonParser {
public:
	JsonParser();
	~JsonParser();
	bool parse(const Measure& measure, const std::wstring& jsonString);
	std::wstring query(const Measure& measure, const std::wstring& key, int index);

private:
	void freeJson();

private:
	std::wstring mContent;
	cJSON* mJson;
	bool mValidResult;
	CRITICAL_SECTION mCriticalSection;
};

#endif // !RM_PLUGIN_JSONPARSER_H