#include "stdafx.h"
#include "JsonParser.h"
#include <vector>
#include "Converter.h"
#include "Measure.h"
#include "Lock.h"

JsonParser::JsonParser() :mJson(NULL), mValidResult(false), mContent(L"") {
	InitializeCriticalSection(&mCriticalSection);
}

JsonParser::~JsonParser() {
	DeleteCriticalSection(&mCriticalSection);
}

bool JsonParser::parse(const Measure& measure, const std::wstring& jsonString) {
	AutoLock autoLock(&mCriticalSection);
	if (mContent.compare(jsonString) == 0 && mJson != NULL) {
		// 当传递进来的字符串与当前Json对象中已经解析了的内容一致的时候就不重复解析Json
		return true;
	}
	mContent = jsonString;
	freeJson();
	mValidResult = false;
	std::string js = Converter::unicodeToUTF8(jsonString);
	mJson = cJSON_Parse(js.c_str());
	if (!mJson) {
		fwprintf(stderr, L"CNWeather.dll: Parse json failed");
		return false;
	}
	cJSON* success = cJSON_GetObjectItem(mJson, "success");
	if (success != NULL && strcmp(success->valuestring, "0") == 0) {
		mValidResult = false;
		cJSON* msg = cJSON_GetObjectItem(mJson, "msg");
		char* msgText = msg->valuestring == NULL ? "" : msg->valuestring;
		fprintf(stderr, "CNWeather.dll: %s\n", msgText);
	} else {
		mValidResult = true;
	}
	return true;
}

std::wstring JsonParser::query(const Measure& measure, const std::wstring& key, int index) {
	AutoLock autoLock(&mCriticalSection);
	if (!mJson || !mValidResult) {
		return L"";
	}
	index = max(index, 0);
	std::string utf8Key = Converter::unicodeToUTF8(key);
	std::wstring resultString(L"");
	cJSON* result = cJSON_GetObjectItem(mJson, "result");
	if (result == NULL) {
		return resultString;
	} else {
		cJSON* keyJson = NULL;
		if (result->type == cJSON_Array) {
			int arrayLen = cJSON_GetArraySize(result);
			if (index >= arrayLen) {
				fprintf(stderr, "CNWeather.dll: Not so many item in the array.\n");
				return resultString;
			}
			cJSON* item = cJSON_GetArrayItem(result, index);
			keyJson = cJSON_GetObjectItem(item, utf8Key.c_str());
		} else if (result->type == cJSON_Object) {
			keyJson = cJSON_GetObjectItem(result, utf8Key.c_str());
		}

		if (keyJson == NULL) {
			// 未查询到指定项
			fwprintf(stderr, L"CNWeather.dll: Unable to find the key \"%s\"\n", key.c_str());
			return resultString;
		}
		resultString = Converter::utf8ToUnicode(keyJson->valuestring);
	}
	return resultString;
}

void JsonParser::freeJson() {
	if (mJson != NULL) {
		cJSON_Delete(mJson);
		mJson = NULL;
	}
}
