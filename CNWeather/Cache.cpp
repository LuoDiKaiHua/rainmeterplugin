#include "stdafx.h"
#include "Cache.h"

#include <time.h>
#include "Lock.h"

Cache::Cache() :mResTime(0) {
}

Cache::~Cache() {
}

time_t Cache::getResTime() const {
	return mResTime;
}

void Cache::setResTime(time_t resTime) {
	mResTime = resTime;
}

// ##################################### DiskCache #####################################

DiskCache::DiskCache() :mCacheDir(""), mEnableCache(false) {
	InitializeCriticalSection(&mCriticalSection);
	size_t requiredSize;
	getenv_s(&requiredSize, nullptr, 0, "TEMP");
	if (requiredSize == 0) {
		mEnableCache = false;
		fwprintf(stderr, L"CNWeather.dll: Unable to find env - %%TEMP%%, so disable the disk cache");
	} else {
		char* buff = new char[requiredSize];
		memset(buff, 0, sizeof(char) * requiredSize);
		getenv_s(&requiredSize, buff, requiredSize, "TEMP");
		mCacheDir = buff;
		delete[] buff;
		mEnableCache = true;
	}
}

DiskCache::~DiskCache() {
	DeleteCriticalSection(&mCriticalSection);
}

std::wstring DiskCache::read(const std::string & key) {
	AutoLock autoLock(&mCriticalSection);
	if (!mEnableCache) {
		return L"";
	}
	std::string path = buildPath(key);

	std::wstring result = L"";
	FILE* pCacheFile = fopen(path.c_str(), "rb");
	if (NULL != pCacheFile) {
		time_t saveTime = 0;
		if (!fseek(pCacheFile, 0, SEEK_END)) {
			long fileLen = ftell(pCacheFile);
			fseek(pCacheFile, 0, SEEK_SET);
			if (fread(&saveTime, sizeof(saveTime), 1, pCacheFile) == 1) {
				setResTime(saveTime);
				if (isCacheLive(saveTime)) {
					char* buff = new char[fileLen];
					memset(buff, 0, sizeof(char) * fileLen);
					if (fread_s(buff, fileLen, sizeof(char), fileLen, pCacheFile)) {
						result = (wchar_t*)buff;
					}
					delete[] buff;
				}
			}
		}
	}
	if (NULL != pCacheFile) {
		fclose(pCacheFile);
	}
	return result;
}

bool DiskCache::write(const std::string & key, std::wstring & content) {
	AutoLock autoLock(&mCriticalSection);
	if (!mEnableCache) {
		return false;
	}
	std::string path = buildPath(key);
	FILE* pCacheFile = fopen(path.c_str(), "wb");
	if (NULL != pCacheFile) {
		time_t t = time(0);
		setResTime(t);
		fwrite(&t, sizeof(t), 1, pCacheFile);
		fwrite(content.data(), sizeof(wchar_t), wcslen(content.data()), pCacheFile);
	}
	if (NULL != pCacheFile) {
		fclose(pCacheFile);
	}
	return true;
}

bool DiskCache::isCacheLive(time_t saveTime) const {
	time_t t = time(0);
	time_t diff = t - saveTime;
	return diff <= 3600;
}

std::string DiskCache::buildPath(const std::string & key) const {
	std::string path = mCacheDir;
	path.append("\\").append(key);
	return path;
}