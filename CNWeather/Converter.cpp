#include "stdafx.h"
#include "Converter.h"

std::wstring Converter::utf8ToUnicode(const std::string& source) {
	std::wstring unicodeStr;
	int strLen = source.length();

	int buffLen = MultiByteToWideChar(CP_UTF8, 0, source.c_str(), strLen, nullptr, 0);
	if (buffLen > 0) {
		unicodeStr.resize(buffLen);
		MultiByteToWideChar(CP_UTF8, 0, source.c_str(), strLen, &unicodeStr[0], buffLen);
	}
	return unicodeStr;
}

std::string Converter::unicodeToUTF8(const std::wstring& source) {
	std::string utf8Str;
	int strLen = source.length();

	int buffLen = WideCharToMultiByte(CP_UTF8, 0, source.c_str(), strLen, nullptr, 0, nullptr, nullptr);
	if (buffLen > 0) {
		utf8Str.resize(buffLen);
		WideCharToMultiByte(CP_UTF8, 0, source.c_str(), strLen, &utf8Str[0], buffLen, nullptr, nullptr);
	}
	return utf8Str;
}