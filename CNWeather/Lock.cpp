#include "stdafx.h"
#include "Lock.h"

// ##################################### Auto Lock ##################################

AutoLock::AutoLock(CRITICAL_SECTION *pCriticalSection) :mCriticalSection(pCriticalSection) {
	EnterCriticalSection(mCriticalSection);
}

AutoLock::~AutoLock() {
	LeaveCriticalSection(mCriticalSection);
}