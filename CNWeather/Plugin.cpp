// CNWeather.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#ifdef WIN64
#pragma comment(lib, ".\\x64\\Rainmeter.lib")
#else
#pragma comment(lib, ".\\x32\\Rainmeter.lib")
#endif // WIN32

#include <vector>
#include "RainmeterAPI.h"
#include "Measure.h"

PLUGIN_EXPORT void Initialize(void** data, void* rm) {
	Measure* measure = new ChildMeasure;
	measure->initialize(rm);
	*data = measure;
}

PLUGIN_EXPORT void Reload(void* data, void* rm, double* maxValue) {
	Measure* measure = reinterpret_cast<Measure*>(data);
	measure->reload(rm, maxValue);
}

PLUGIN_EXPORT LPCWSTR GetString(void* data) {
	Measure* measure = reinterpret_cast<Measure*>(data);
	static std::wstring result;
	result = measure->getString();
	return result.c_str();
}

PLUGIN_EXPORT void Finalize(void* data) {
	Measure* measure = reinterpret_cast<Measure*>(data);
	measure->finalize();
	delete measure;
}