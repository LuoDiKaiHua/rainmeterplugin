#include "stdafx.h"
#include "Measure.h"

#include <process.h>
#include "RainmeterAPI.h"
#include "Converter.h"
#include "Lock.h"
#include "MD5.h"

template<typename T>
static std::wstring toString(T value) {
	std::wstringstream ws;
	ws << value;
	return ws.str();
}

// ##################################### Measure #####################################

Measure::Measure() :mRm(nullptr), mSkin(nullptr), mName(L""), mDefaultValue(L"") {
}

Measure::~Measure() {
}

void Measure::initialize(void* rm) {
	mRm = rm;
	mSkin = RmGetSkin(rm);
	mName = RmGetMeasureName(rm);
}

void Measure::reload(void * rm, double * maxValue) {
	mRm = rm;
	mDefaultValue = RmReadString(rm, L"DefaultValue", L"");
}

double Measure::update() {
	return 0.0;
}

std::wstring Measure::getString() {
	return mDefaultValue;
}

void Measure::finalize() {
}

bool Measure::operator==(const Measure & measure) const {
	return &measure == this || measure.mName == this->mName;
}

std::wstring Measure::getName() const {
	return mName;
}

void* Measure::getSkin() const {
	return mSkin;
}

void* Measure::getRm() const {
	return mRm;
}

// ##################################### ParentMeasure #####################################

ParentMeasureList ParentMeasure::sMeasureList;
const wchar_t* const ParentMeasure::sTodayPtr = L"today";
const wchar_t* const ParentMeasure::sFuturePtr = L"future";
const wchar_t* const ParentMeasure::sAqiPtr = L"pm25";
const wchar_t* const ParentMeasure::sIPApi = L"http://httpbin.org/ip";
std::wstring ParentMeasure::sIPAddress = L"";

ParentMeasure::ParentMeasure()
	:mChild(nullptr), mAppKey(L""), mSign(L""), mCityCode(L""),
	mThreadHandle(NULL), mCache(new DiskCache), mHttpClient(nullptr), mLastReloadTime(0) {
	sMeasureList.push_back(this);
	InitializeCriticalSection(&mCriticalSection);
}

ParentMeasure::~ParentMeasure() {
	ParentMeasureList::iterator iter = std::find(sMeasureList.begin(), sMeasureList.end(), this);
	sMeasureList.erase(iter);

	if (mThreadHandle != NULL) {
		AutoLock autoLock(&mCriticalSection);
		if (mThreadHandle != NULL) {
			TerminateThread(mThreadHandle, 0);
			mThreadHandle = NULL;
		}
	}

	delete mCache;
	mCache = nullptr;
	if (mHttpClient != nullptr) {
		delete mHttpClient;
		mHttpClient = nullptr;
	}
	DeleteCriticalSection(&mCriticalSection);
}

void ParentMeasure::initialize(void * rm) {
	Measure::initialize(rm);
	mAppKey = RmReadString(rm, L"AppKey", L"10003");
	mSign = RmReadString(rm, L"Sign", L"b59bc3ef6191eb9f747dd4e83c99f2a4");
}

void ParentMeasure::reload(void* rm, double* maxValue) {
	Measure::reload(rm, maxValue);
	std::wstring tempCityCode = RmReadString(rm, L"CityCode", L"");
	bool updateRightNow = tempCityCode.compare(mCityCode) != 0;
	mCityCode = tempCityCode;
	time_t currTime = time(0);
	unsigned int id;
	if (mThreadHandle == NULL && (updateRightNow || (currTime - mLastReloadTime >= 30))) {
		mLastReloadTime = currTime;
		HANDLE threadHandle = (HANDLE)_beginthreadex(nullptr, 0, networkThreadProc, this, 0, &id);
		if (threadHandle) {
			mThreadHandle = threadHandle;
		}
	}
}

std::wstring ParentMeasure::getString() {
	return Measure::getString();
}

void ParentMeasure::finalize() {
	Measure::finalize();
}

ChildMeasure* ParentMeasure::getChild() const {
	return mChild;
}

void ParentMeasure::setChild(ChildMeasure * childMeasure) {
	mChild = childMeasure;
}

ParentMeasure* ParentMeasure::findMeasure(std::wstring & measureName, void* skin) {
	for (ParentMeasureList::const_iterator iter = sMeasureList.begin(); iter != sMeasureList.end(); ++iter) {
		ParentMeasure* measure = *iter;
		if (measure->getName() == measureName && measure->getSkin() == skin) {
			return *iter;
		}
	}
	return nullptr;
}

ParentMeasure* ParentMeasure::findMeasure(const ChildMeasure* measureChild) {
	for (ParentMeasureList::const_iterator iter = sMeasureList.begin(); iter != sMeasureList.end(); ++iter) {
		if ((*iter)->getChild() == measureChild) {
			return *iter;
		}
	}
	return nullptr;
}

JsonParser* ParentMeasure::getParser(std::wstring& weatherType) {
	LPCWSTR weatherTypePtr = weatherType.c_str();
	if (_wcsicmp(sTodayPtr, weatherTypePtr) == 0) {
		return &mTodayParser;
	} else if (_wcsicmp(sFuturePtr, weatherTypePtr) == 0) {
		return &mFutureParser;
	} else if (_wcsicmp(sAqiPtr, weatherTypePtr) == 0 || _wcsicmp(L"aqi", weatherTypePtr) == 0) {
		return &mAqiParser;
	}
	return nullptr;
}

time_t ParentMeasure::getResTime() const {
	return mCache->getResTime();
}

void ParentMeasure::updateWeatherData(std::wstring weatherType) {
	std::wstring cityCode = mCityCode.empty() ? sIPAddress : mCityCode;
	std::wstring url(L"http://api.k780.com/?app=weather." + weatherType + L"&weaid=" + cityCode + L"&&appkey=" + mAppKey + L"&sign=" + mSign + L"&format=json");

	std::string cacheKey = MD5::MD5(Converter::unicodeToUTF8(url)).hexdigest();

	std::wstring cacheResult = mCache->read(cacheKey);
	JsonParser* parser = getParser(weatherType);
	if (!cacheResult.empty()) {
		parser->parse(*this, cacheResult);
	} else {
		if (mHttpClient == nullptr) {
			mHttpClient = new WinHttpClient(url);
		} else {
			mHttpClient->UpdateUrl(url);
		}
		if (mHttpClient->SendHttpRequest()) {
			std::wstring content = mHttpClient->GetResponseContent();
			mCache->write(cacheKey, content);
			parser->parse(*this, content);
		}
	}
}

unsigned ParentMeasure::networkThreadProc(void * pParam) {
	ParentMeasure* measure = reinterpret_cast<ParentMeasure*>(pParam);
	if (measure->mCityCode.empty() && ParentMeasure::sIPAddress.empty()) {
		// 未设置城市代码，自动获取当前外网ip地址
		if (measure->mHttpClient == nullptr) {
			measure->mHttpClient = new WinHttpClient(ParentMeasure::sIPApi);
		} else {
			measure->mHttpClient->UpdateUrl(ParentMeasure::sIPApi);
		}
		if (measure->mHttpClient->SendHttpRequest()) {
			std::wstring jsonIp = measure->mHttpClient->GetResponseContent();
			cJSON* json = cJSON_Parse(Converter::unicodeToUTF8(jsonIp).c_str());
			if (json != NULL) {
				cJSON* origin = cJSON_GetObjectItem(json, "origin");
				std::wstring ip = Converter::utf8ToUnicode(origin->valuestring);
				if (!ip.empty()) {
					measure->sIPAddress = ip;
				}
				cJSON_Delete(json);
			}
		}
	}

	measure->updateWeatherData(measure->sTodayPtr);
	measure->updateWeatherData(measure->sFuturePtr);
	measure->updateWeatherData(measure->sAqiPtr);

	AutoLock autoLock(&measure->mCriticalSection);
	CloseHandle(measure->mThreadHandle);
	measure->mThreadHandle = NULL;
	return 0;
}

// ##################################### ChildMeasure #####################################

ChildMeasure::ChildMeasure()
	:mParent(nullptr), mWeatherType(L""), mDay(0),
	mItemName(L""), mResTime(0), mParamChanged(false), mStringResult(L"") {
}

ChildMeasure::~ChildMeasure() {
}

void ChildMeasure::setParent(ParentMeasure* measure) {
	mParent = measure;
}

ParentMeasure* ChildMeasure::getParent() const {
	return mParent;
}

void ChildMeasure::initialize(void* rm) {
	Measure::initialize(rm);
	LPCWSTR parentName = RmReadString(rm, L"ParentMeasure", L"");
	if (!*parentName) {
		ParentMeasure* parent = new ParentMeasure;
		parent->initialize(rm);
		setParent(parent);
		parent->setChild(this);
	} else {
		ParentMeasure* parent = ParentMeasure::findMeasure(std::wstring(parentName), getSkin());
		if (parent == nullptr) {
			RmLogF(rm, LOG_ERROR, L"CNWeather.dll: Could not find parent by name %s", parentName);
			return;
		}
		this->setParent(parent);
	}
}

void ChildMeasure::reload(void* rm, double* maxValue) {
	Measure::reload(rm, maxValue);
	ParentMeasure* parent = this->getParent();
	if (parent != nullptr && parent->getChild() == this) {
		parent->reload(rm, maxValue);
	}

	std::wstring tempWeatherType = RmReadString(rm, L"WeatherType", L"Today");
	int tempDay = RmReadInt(rm, L"Day", 0);
	std::wstring tempItemName = RmReadString(rm, L"ItemName", L"cityno");
	if (_wcsicmp(tempWeatherType.c_str(), mWeatherType.c_str()) != 0
		|| tempDay != mDay
		|| _wcsicmp(tempItemName.c_str(), mItemName.c_str()) != 0) {
		mParamChanged = true;
	}
	mWeatherType = tempWeatherType;
	mDay = tempDay;
	mItemName = tempItemName;
}

std::wstring ChildMeasure::getString() {
	time_t parentResTime = mParent->getResTime();
	if (wcsicmp(L"updatetime", mItemName.c_str()) == 0) {
		return toString(parentResTime);
	}
	if (mResTime == parentResTime && !mParamChanged) {
		return mStringResult.empty() ? mDefaultValue : mStringResult;
	}
	mResTime = parentResTime;
	mParamChanged = false;
	JsonParser* parser = mParent->getParser(mWeatherType);
	if (parser == nullptr) {
		RmLogF(getRm(), LOG_ERROR, L"CNWeather.dll: Error WeatherType: %s", mWeatherType.c_str());
		return L"";
	}
	mStringResult = parser->query(*this, mItemName, mDay);
	return mStringResult.empty() ? mDefaultValue : mStringResult;
}

void ChildMeasure::finalize() {
	Measure::finalize();
	ParentMeasure* parent = this->getParent();
	if (parent != nullptr && parent->getChild() == this) {
		parent->finalize();
		delete parent;
		this->setParent(nullptr);
	}
}