#pragma once

#ifndef RM_PLUGIN_MEASURE_H_
#define RM_PLUGIN_MEASURE_H_

#include <string>
#include <vector>
#include "Cache.h"
#include <WinHttpClient.h>
#include "JsonParser.h"

class Measure {
protected:
	void* mRm;
	void* mSkin;
	std::wstring mName;
	std::wstring mDefaultValue;

public:
	Measure();
	Measure(const Measure&) = delete;
	virtual ~Measure();

	virtual void initialize(void* rm);
	virtual void reload(void* rm, double* maxValue);
	virtual double update();
	virtual std::wstring getString();
	virtual void finalize();

	bool operator == (const Measure& measure) const;

	std::wstring getName() const;
	void* getSkin() const;
	void* getRm() const;
};

class ParentMeasure;
class ChildMeasure;
typedef std::vector<ParentMeasure*> ParentMeasureList;

class ParentMeasure :public Measure {
public:
	ParentMeasure();
	~ParentMeasure();
	ParentMeasure(const ParentMeasure&) = delete;

	virtual void initialize(void* rm) override;
	virtual void reload(void* rm, double* maxValue) override;
	virtual std::wstring getString() override;
	virtual void finalize() override;

	ChildMeasure* getChild() const;
	void setChild(ChildMeasure* childMeasure);

	JsonParser* getParser(std::wstring& weatherType);
	time_t getResTime() const;

	static ParentMeasure* findMeasure(std::wstring& measureName, void* skin);
	static ParentMeasure* findMeasure(const ChildMeasure* measureChild);

private:
	void updateWeatherData(std::wstring weatherType);
	static unsigned __stdcall networkThreadProc(void* pParam);

private:
	static const wchar_t* const sTodayPtr;
	static const wchar_t* const sFuturePtr;
	static const wchar_t* const sAqiPtr;
	static const wchar_t* const sIPApi;
	static std::wstring sIPAddress;
	CRITICAL_SECTION mCriticalSection;
	ChildMeasure* mChild;
	std::wstring mAppKey;
	std::wstring mSign;
	std::wstring mCityCode;
	Cache* mCache;
	volatile HANDLE mThreadHandle;
	WinHttpClient* mHttpClient;
	JsonParser mTodayParser, mFutureParser, mAqiParser;
	time_t mLastReloadTime;

	static ParentMeasureList sMeasureList;
};


class ChildMeasure :public Measure {
public:
	ChildMeasure();
	~ChildMeasure();

	virtual void initialize(void* rm) override;
	virtual void reload(void* rm, double* maxValue) override;
	virtual std::wstring getString() override;
	virtual void finalize() override;

	void setParent(ParentMeasure* measure);
	ParentMeasure* getParent() const;
private:
	ParentMeasure* mParent;
	std::wstring mWeatherType;
	std::wstring mItemName;
	int mDay;
	std::wstring mStringResult;
	bool mParamChanged;
	time_t mResTime;
};

#endif