#pragma once

#ifndef RMPLUGIN_STRINGCENVERTER_H
#define RMPLUGIN_STRINGCENVERTER_H

#include <string>
#include <sstream>

class Converter {
public:
	Converter() = delete;
	~Converter() = delete;

	static std::wstring utf8ToUnicode(const std::string& source);
	static std::string unicodeToUTF8(const std::wstring& source);

};

#endif // RMPLUGIN_STRINGCENVERTER_H
